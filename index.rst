.. Gradient Pricing documentation master file, created by
   sphinx-quickstart on Sat Aug 27 15:51:38 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Gradient Pricing's documentation!
============================================

Gradient is an easy way to create pricing experiments that are as invisible to end users as possible.

Gradient minimizes the chance that users notice your pricing experiment by hashing on data available at the time of request (user key, session id, ip address). Users who have visited before will continue to see the same price in subsequent visits.

If another visitor comes later that shares any subset of information (user key, session id, or ip address) as any previous visitor, they will see the same price as the matching previous visitor. This is to help ensure that pricing experiments are as invisible to users as possible.

While it’s not necessary to have all three pieces of user information when making a price request to Gradient, having more information minimizes the chance of the same user or users near each other seeing different prices.  If none of the three pieces of user information are provided in the price request, the control price is returned and no experiment data is logged.

This document will show you how to create and set up your first pricing experiment.


Creating your first experiment
---------------

Go to the create experiment page to set up your first pricing experiment.  Here you will be prompted to set control and test prices. Once you have decided your prices, click `Create` on the bottom of the page.

Once you’ve clicked create, you’ll be redirected to your experiment dashboard. You can view all created experiments via the experiment dashboard. When you are ready to start your experiment, go ahead and click the toggle next to the experiment name to start the experiment. Not started or paused experiments will return the control price, and will not log any experiment data for analysis.

Note that it is dangerous to pause your experiment before you are ready to conclude your experiment. Once an experiment is paused, all previous users who saw a variation price before will now begin to see the control price. The more often you start/stop an experiment, the more likely your users will notice that you are running a pricing experiment.

Integration
---------------

Get your api key and experiment id from the experiment dashboard.

.. image:: api_key.png


Fetch request level user info (user key, session id, ip address)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Django (Python)

.. code:: python

    from ipware.ip import get_ip
    import uuid

    def get_user_info_from_request(request):
        user_key = request.user.id
        ip = get_ip(request)

        # Get gradient_session_id if available - if not create one
        gradient_session_id = request.COOKIES.get('gradient_session_id')
        if not gradient_session_id:
            gradient_session_id = uuid.uuid4()

        return {'ip': ip, 'session_id': gradient_session_id, 'user_key': user_key}

Fetching the price to display for the user request
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Django (Python)

.. code:: python


    def home(request):

        headers = {'apikey': GRADIENT_API_KEY}
        data = get_user_info_from_request(request)
        data['product_id'] = 11

        r = requests.get(
            'https://www.gradientpricing.com/api/visit', headers=headers,  params=data
        )
        price = r.json()['price']

        response = render(request, 'home.html', {'price': price})

        # Set gradient_session_id cookie
        response.set_cookie('gradient_session_id', data['session_id'], max_age=365*7*24*60*60)
        return response

Recording the conversion event
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Django (Python)

.. code:: python

    def checkout(request):

        data = get_user_info_from_request(request)
        data['product_id'] = 11

        r = requests.post(
            'https://www.gradientpricing.com/api/conversion', headers=headers,  data=data
        )

